# 💣 Security Issue: `REPLACE_ME` is happening


<!-- overview of the security issue -->


## Security Categorisation


- [ ] Information leak
- [ ] Unauthorised data access
- [ ] Unauthenticated data access
- [ ] Application breach
- [ ] Secrets found elsewhere


## Steps to Reproduce


1. <!-- eg. login -->
2. <!-- eg. go to page X -->


## Relevant Attachments


<!-- attach any other useful content here -->


## Technical analysis


<!-- if you have an idea of why this is happening, here is the place to write it -->


/shrug shit just hit the fan

/confidential
/label ~Security
/assign me


<!-- ref: https://docs.gitlab.com/ee/user/project/quick_actions.html -->

