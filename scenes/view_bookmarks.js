const Telegraf = require('telegraf')
const external_api_caller = require('../api/api')
const Markup = require('telegraf/markup')
const WizardScene = require('telegraf/scenes/wizard')
const Stage = require('telegraf/stage')
const session = require('telegraf/session')
const axios = require('axios')

const bookmarks_viewer = new WizardScene(
    'view_bookmarks',
    ctx => {
      ctx.reply(
        'Do you want to add or remove skills?',
        Markup.inlineKeyboard([
          Markup.callbackButton('Add Skills', 'Add'),
          Markup.callbackButton('Remove Skills', 'Remove'),
          Markup.callbackButton('No thanks', 'No')
        ]).extra()
      ) 
      return ctx.wizard.next()
    },
    
    )

    module.exports = bookmarks_viewer