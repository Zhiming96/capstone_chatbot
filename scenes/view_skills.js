require('dotenv').config()
const Telegraf = require('telegraf')
const external_api_caller = require('../api/api')
const Markup = require('telegraf/markup')
const WizardScene = require('telegraf/scenes/wizard')
const Stage = require('telegraf/stage')
const session = require('telegraf/session')
const axios = require('axios')


const skill_viewer = new WizardScene(
    'view_skills',
    ctx => {
      console.log(ctx.scene.ctx)
      ctx.reply(
        'Do you want to add or remove skills?', 
        Markup.inlineKeyboard([ 
          Markup.callbackButton('Add Skills', 'Add'),
          Markup.callbackButton('Remove Skills', 'Remove'),
          Markup.callbackButton('No thanks', 'No')
        ]).extra()
      ) 
      return ctx.wizard.next()
    },
    ctx => { 
      const next_action = (ctx.wizard.state.next_action =
        (ctx.update.callback_query? (ctx.update.callback_query.data ? ctx.update.callback_query.data : 'No'): 'No'))
      if(next_action == 'No') {
        ctx.reply('Okay :(')
        return ctx.scene.leave()
      } else if (next_action == 'Add') {
  //ctx.reply('Which skill would you like to add? (e.g. Software Engineer)')
  ctx.scene.leave()
  ctx.scene.enter('add_skills')
  //return ctx.scene.leave()
  //for now is free text. Will attempt to code matching system another day
      } else if (next_action == 'Remove') {
        ctx.reply('Which skill (number) would you like to remove?')
      
      }
      return ctx.wizard.next()
    },
    ctx => {
      if (ctx.wizard.state.next_action == 'Add') {
        ctx.wizard.state.skill_add = ctx.message.text
        var api = process.env.BACKEND_SERVER + 'skills/add'
        var body = {
          skill_add: ctx.wizard.state.skill_add,
          telegram_id: ctx.message.from.id,
          source: 'telegram'
        }
        external_api_caller
        .post_api(api,body)
        .then(response => {
       
console.log(response.response_code)
  if (response.response_code == 200) {
    ctx.reply('Skill ' + ctx.wizard.state.skill_add + ' added successfully!')
  } else if (response.response_code != 500) {
    ctx.reply(response.response_message)
  } else {
    ctx.reply('Error adding skill')
  }
        }).catch(error => {
          ctx.reply('Error adding skill')
        })
      
  
      
      } else if (ctx.wizard.state.next_action == 'Remove') {
        ctx.wizard.state.skill_remove = ctx.message.text
        var api = process.env.BACKEND_SERVER + 'skills/remove'
        var body = {
          skill_remove: ctx.wizard.state.skill_remove - 1,
          telegram_id: ctx.message.from.id,
          source: 'telegram'
        }
        external_api_caller
        .post_api(api,body)
        .then(response => { 
          console.log('omg')
console.log(response.response_code)
  if (response.response_code == 200) {
    ctx.reply('Skill ' + ctx.wizard.state.skill_remove + ' removed successfully!')
  }
        }).catch(error => {
          ctx.reply('Error removing skill')
        })
        return ctx.scene.leave()
      
  
      }
      return ctx.wizard.next()
    },
    ctx => {
      if (ctx.wizard.state.next_action == 'Add') {

      } else if (ctx.wizard.state.next_action == 'Remove') {
      
    
      
    }
  }
  )

  module.exports = skill_viewer
