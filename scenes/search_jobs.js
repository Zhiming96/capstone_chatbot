const Telegraf = require('telegraf')
const external_api_caller = require('../api/api')
const Markup = require('telegraf/markup')
const WizardScene = require('telegraf/scenes/wizard')
const Stage = require('telegraf/stage')
const session = require('telegraf/session')
const axios = require('axios')

const job_searcher = new WizardScene(
    'search_jobs',
    ctx => {
      ctx.replyWithMarkdown('For more search functions, visit our web app!')
      ctx.replyWithMarkdown('*What keyword to search for? (e.g. engineer)*')
      return ctx.wizard.next()
    },
    async ctx => {
    
      ctx.wizard.state.keyword = ctx.message.text
      await   ctx.replyWithMarkdown(`*Searching for jobs with keyword:* ${ctx.wizard.state.keyword}`)
      await   ctx.replyWithMarkdown('*What\'s the minimum salary you\'re expecting? (e.g. 1500)*') // sometimes appear before previous message. To debug
      return ctx.wizard.next()
    },
    async ctx => {
      ctx.wizard.state.minimum_salary = ctx.message.text
     
while(isNaN(parseInt(ctx.wizard.state.minimum_salary))){
  await   ctx.replyWithMarkdown(`*A numerical input is required! (e.g. 1500)*`)
  return  async (ctx) => {
    ctx.wizard.state.minimum_salary = ctx.message.text
  }
}
      await ctx.replyWithMarkdown(
        // need to perform validation

        `*Search for jobs with minimum salary:* ${ctx.wizard.state.minimum_salary}`
     
      )
      // need to account for double typing
      await  ctx.replyWithMarkdown(
        '*What type of employment are you looking for?*',
        Markup.inlineKeyboard([
          Markup.callbackButton('Full Time', 'Full Time'),
          Markup.callbackButton('Part Time', 'Part Time')
        ]).extra()
      )
      return ctx.wizard.next()
    },
    ctx => {
      const employment_type = (ctx.wizard.state.employment_type =
        ctx.update.callback_query.data)
      var message = ''
      var additional_param = ''

      additional_param += ctx.wizard.state.keyword
        ? '?keyword=' + ctx.wizard.state.keyword
        : '?'
      additional_param +=
        (ctx.wizard.state.keyword ? '&' : '') +
        (ctx.wizard.state.minimum_salary
          ? 'salary=' + ctx.wizard.state.minimum_salary
          : '')
      additional_param +=
        (ctx.wizard.state.keyword || ctx.wizard.state.minimum_salary ? '&' : '') +
        (ctx.wizard.state.employment_type
          ? 'employmentTypes=' + ctx.wizard.state.employment_type
          : '')
      additional_param += 
        (ctx.wizard.state.keyword ||
        ctx.wizard.state.minimum_salary ||
        ctx.wizard.state.employment_type 
          ? '&'
          : '') + 'limit=3&page=0&source=telegram&' // realized the previous conditional abit unnecessary cuz of this but aiya for now liddat bah
      // console.log(api)
      additional_param += 'telegram_id=' + ctx.update.callback_query.from.id 
      var api = process.env.BACKEND_SERVER + 'jobs/search' + additional_param
      console.log(api)
var save_search =  process.env.BACKEND_SERVER + 'search/save' 
var search_body = {
  telegram_id:  ctx.update.callback_query.from.id,
  keyword: ctx.wizard.state.keyword,
  employment_type:ctx.wizard.state.employment_type,
  minimum_salary: ctx.wizard.state.minimum_salary,
  
//to add more eventually 
}
     
          external_api_caller
          .call_api(api)
          .then(response => {
            // important info
            // console.log(response);
            if(response.results.length != 0) {
            message += '*Title:* ' + response.results[0].title + '\n'
            message += '*Job Type:* '
            message += response.results[0].employmentTypes[0].employmentType + '\n'
          console.log('2123')
          message += '*Company:* ' 
          message +=  response.results[0].metadata.isHideEmployerName
            ? 'Employer Undisclosed\n'
            : (posted_company = response.results[0].postedCompany.name) + '\n'
          console.log('3123')
          message += '*Salary:* ' 
          message +=  response.results[0].metadata.isHideSalary
            ? 'Salary Undisclosed\n'
            : (salary =
                'S$' +
                response.results[0].salary.minimum + 
                ' - ' +
                'S$' +
                response.results[0].salary.maximum) + '\n'
          console.log('4123')
          
          message += '\n\n'
  //         message = (title = response.results[0].title) + '\n'
  // console.log(response.results[0].employmentTypes[0].employmentType) // only 1 for this array?
  // message +=
  //   response.results[0].employmentTypes[0].employmentType + '\n'
  // console.log('2123')
  // message += response.results[0].metadata.isHideEmployerName
  //   ? 'Employer Undisclosed\n'
  //   : (posted_company = response.results[0].postedCompany.name) + '\n'
  // console.log('3123')
  // message += response.results[0].metadata.isHideSalary
  //   ? 'Salary Undisclosed\n' 
  //   : (salary =
  //       'S$' +
  //       response.results[0].salary.minimum +
  //       ' - ' + 
  //       'S$' +
  //       response.results[0].salary.maximum) + '\n'
  // console.log('4123')
  // message += '\n\n'
  // console.log(message)
  console.log(response.record_id)
          var record_id = response.record_id
 //  var back_message = 'sj_bck_' +record_id + '_0' 
   var next_message = 'sj_nxt_' + record_id +'_0' 
   
         ctx.replyWithMarkdown(message, Markup.inlineKeyboard([
         //  Markup.callbackButton('<', back_message),
           Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
           Markup.callbackButton('>', next_message),
           Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
          ]).extra())
  /*
          for (job in response.results) {
          console.log(job)
          message += (title = response.results[job].title) + '\n'
          console.log(response.results[job].employmentTypes[0].employmentType) // only 1 for this array?
          message +=
            response.results[job].employmentTypes[0].employmentType + '\n'
          console.log('2123')
          message += response.results[job].metadata.isHideEmployerName
            ? 'Employer Undisclosed\n'
            : (posted_company = response.results[job].postedCompany.name) + '\n'
          console.log('3123')
          message += response.results[job].metadata.isHideSalary
            ? 'Salary Undisclosed\n'
            : (salary =
                'S$' +
                response.results[job].salary.minimum +
                ' - ' +
                'S$' +
                response.results[job].salary.maximum) + '\n'
          console.log('4123')
          message += '\n\n'
          console.log(message)
           }
  */
          //    message +=   (status = response.results[job].status.jobStatus) + "\n";
  
          // message +=   (category = response.results[job].categories[job].category) + "\n"; //might have more, for now take first one
          // message +=   (employment_type = response.results[job].employmentTypes[job].employmentType) + "\n";
        
          // message +=   (position_level = response.results[job].positionLevels[job].position) + "\n";
          //
  
          //         //additional info
          //         var skills = [];
          //         response.results[job].skills.forEach(function(skill) {
          // skills.push(skill.skill);
          //         })
          //         var description = response.results[job].description;
          //         var minimum_years_experience = response.results[job].minimumYearsExperience;
          //         var other_requirements = response.results[job].otherRequirements;
          //         var working_hours = response.results[job].workingHours;
          //         var number_of_vacancies = response.results[job].numberOfVacancies;
  
          // res.json(response)
  
      
          } else {
            ctx.replyWithMarkdown('*No jobs found. Please try a different search!*')

          }
            
          })
          .catch(error => {
            console.log(error)
            ctx.reply('Error retrieving jobs')
          })
        

      //  return ctx.wizard.next()
      return ctx.scene.leave()
    } 
  )
  module.exports = job_searcher