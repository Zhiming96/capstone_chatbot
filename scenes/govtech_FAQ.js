const dialogflow = require("dialogflow");
const uuid = require("uuid");
const { struct } = require("pb-util");
const { Extra } = require("telegraf");
const WizardScene = require("telegraf/scenes/wizard");
const request = require('request')
const Markup = require("telegraf/markup");


// const app = dialogflow();

/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */

// Create a new session
const sessionId = uuid.v4();

projectId = "testintegration-cpgfin";

var dfReply;
var checkFirst = true;
var count = 0;

//$env:GOOGLE_APPLICATION_CREDENTIALS="C:\capstone\testintegration-cpgfin-401c6993a811.json"

/*Ern Tek's file path
$env:GOOGLE_APPLICATION_CREDENTIALS="C:\Users\ErnTek\Desktop\Capstone\testintegration-cpgfin-a68dfacd7cbd.json
SET GOOGLE_APPLICATION_CREDENTIALS=C:\Users\ErnTek\Desktop\Capstone\testintegration-cpgfin-a68dfacd7cbd.json
*/
let config = {
  credentials : {
  client_email : process.env.GOOGLEDF_EMAIL,
  private_key : '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC6Hlb+m1hBpX0p\nAlrsVQ2/58kKjGpU4PZsnA11NrKVY2zkWLjN4MGA/JS7olM9Nnp++ffqS85hsJvx\nYf48BAVvEQAlkB+qHsCIIsmtM2qGOuvUSrE1vCGrS+qT8hpLYE7zAu+m716JNLoa\nsczQ5caeqQErFjOEfuAcLLmMkPSlznzwZ3SFxZ4K3PP4KERELNjpJZFvQsMcUS15\n5DpWv5ibrWyvfdTky5Wn5/4GsNsIyXD8oJe5rLSao9kDYvjxWmXPtjEQvWomxHkA\npEzi8D2KTzDHyp483uFwTIFyG97NbxqiopZwV0HwMjQEu0siGRBzWYH7FIXgxaQT\nVugwjsDtAgMBAAECggEADCLlVlXn4+P4LN/n8Og87Moe/G6akm1W9E8au411CLPC\nioNUjaw9wfz2b+POXLv8MeJ37ZHC4/DhwlQ7AUvb1Nm9cjWbMvstX9Mw5nfnVthp\nv16dfp0FbweUxM/xMgAWNdd3rCGIvxaumgYFDowlLvxjRJ1+f4HMeFncQZPEODmJ\nyUleiS0f3u3ofcCGnTP0l9GRZVFQqVwkOhDWdn3v9QnvKtfBNiA1qBhq4JA5V1c/\ngiJsOf+mY82HCtPQKn73xBqaPYQNVtQVS3Zj7EUyyrOiVGIE4CVn0gZ3bUKnZajw\nu7w89ZonDWPxqufFclkpj0mAaisMJUZHkkSTMnHrCwKBgQDbvMZ8NcyzBZ68vZq3\nsvA0cPgTiaA437QQeLE0l7k/vRUes4r70cMAna0qeGlnTPL5sEuALFbUmA5UgLyC\ncEetkDOr1vR/+AJNLjFKRCVO3UraURUQfq7mYMg54a9GfMZEhEgR3KK939Ld68RQ\nrLWPpjKt9uW8jMeS0SeVTh34fwKBgQDY1UVEekjycgMYXlg2aV3Xbm8DlGweTyLI\ntQ6wXPfDeGAK2FcfxK8J5P623OWqF8FUM5DxyYvinmZDOT8ZTg6KasxkNiSi9sC5\n4b5gXEq74hisXXf8nY2F3MpuGUzWHgU3ALuUq7hPft2cUGjdZd3z2aNTobuSd3XL\nf1aQIp7wkwKBgQCp7wzlJEohLLh9KHjNlgGz3KXNnkKdmUtM7bnFfYzTISyDKMDU\nTH1tLYOs86A4Nkc2GEPbZO7lzI5QhZrDmYelT37WZw+BI9DL1WxjDd54ZiUsP0Z2\nEjK6kaBnOwTtEzPV6yM9RQ9Knh0aaOnSpi5nYFWTOfyjW5BAF6ZwDhCZiwKBgQDX\nwthrPHdr8LFNxUtn0U8UuIn0+PLg8gP9zdRxAAF220hOYlnHfdJjOR+QXJyXtY2l\nuK4pOlFczhuaFtPH3VkwuPbrXMj19C1wicYDYrLm5CODGHr1vtz9b+dqyPghM7wi\nsDL3zCvZxrbD/WtrqxrBtDYGWTApnX90YCCFyj13ywKBgFASzKCwM2hjdkDHs7G8\nfJMLoCtmzMrkl7QRc3CAdGiCL1vIy9HI857SDjkIwIozMfXhX9PWt0XXeP1Zr+GZ\nHzWpcg/ZcRpPJfmSa/+aweo1WBH71uQ+nDOG8h17dn3nZKjCZ1gSwMMBJbi5hf3k\nTkaJ7x/md/33qSsLODfI9Hqf\n-----END PRIVATE KEY-----\n'
}
}
const sessionClient = new dialogflow.SessionsClient(config);
async function runSample(projectId, text) {
  // A unique identifier for the given session

  
  const sessionPath = sessionClient.sessionPath(projectId, sessionId);
  // The text query request.
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        // The query to send to the dialogflow agent
        text: text,
        // The language used by the client (en-US)
        languageCode: "en-US"
      }
    }
  };

  // Send request and log result
  console.log("sending request");
  const responses = await sessionClient.detectIntent(request);
  console.log("Detected intent");
  const result = responses[0].queryResult;
  const jsonParameters = JSON.stringify(
    struct.decode(responses[0].queryResult.parameters)
  );
  const parameters = JSON.parse(jsonParameters);

  console.log(`  Query: ${result.queryText}`);
  console.log(`  Response: ${result.fulfillmentText}`);
  dfReply = result.fulfillmentText;
  userQuery = result.queryText;

  if (result.intent) {
    console.log(`  Intent: ${result.intent.displayName}`);
  } else {
    console.log(`  No intent matched.`);
  }
  return parameters;
}

let empty = true;
let schemesReply = "";

function checkIfReplyEmpty() {
  // Checks if replyString is empty.
  if (empty) {
    empty = false;
    schemesReply = schemesReply.concat(
      "Take a look at these schemes available for you :\n\n"
    );
    return schemesReply;
  } else {
    return null;
  }
}

const govTechFAQ = new WizardScene(
  "govTechFAQ",
  ctx => {
    response = runSample(projectId, "What schemes are there?").then(
      parameters => {
        var message = `Hello! We're here to help you look for schemes. But first, we need these information from you - *age*, *employment status*, *citizenship* (Please state if you are PR).\n\nIf you would like to stop Schemes Enquiry, please type 'exit'.`;
        ctx.replyWithMarkdown(message);
        return ctx.wizard.next();
      }
    );
  },

  ctx => {
    ctx.wizard.state.keyword = ctx.message.text;
    if(ctx.wizard.state.keyword.toLowerCase().indexOf("exit") === 0){
      ctx.replyWithMarkdown("Schemes Enquiry has been terminated.");
      ctx.scene.leave()
    }
    response = runSample(projectId, ctx.wizard.state.keyword).then(
      parameters => {
        console.log("This is the response : " + dfReply);
        // console.log(parameters);

        if (typeof dfReply == "string" && dfReply.indexOf("hank") > -1) {
          // 'thank you' only appears once all questions are asked.
          let age = parameters.age;
          let nationality = parameters.nationality;
          let employmentStatus = parameters.employment_type;
          let educationLevel = parameters.education_level;
          let pmetStatus = parameters.pmet;
          console.log(
            age +
              "," +
              nationality +
              "," +
              employmentStatus +
              "," +
              educationLevel +
              "," +
              pmetStatus
          );

          const pmax =
            "https://www.wsg.gov.sg/programmes-and-initiatives/p-max-employer.html\n";
          const csp =
            "https://www.wsg.gov.sg/programmes-and-initiatives/wsg-career-support-programme-individuals.html\n";
          const cTrial =
            "https://www.wsg.gov.sg/programmes-and-initiatives/career-trial-jobseekers.html\n";

          let schemeArray = [pmax, csp, cTrial];

          if (
            age > 16 &&
            nationality == "Singaporean" &&
            employmentStatus == "unemployed"
          ) {
            checkIfReplyEmpty();
            schemesReply = schemesReply.concat(
              "Career Support Scheme - " + schemeArray[1]
            );
            schemesReply = schemesReply.concat(
              "Career Trial Scheme - " + schemeArray[2]
            );
          }
          if (
            (age > 21 && nationality == "Singaporean") ||
            nationality == "Permanent Resident"
          ) {
            if (
              educationLevel == "Diploma and above" ||
              (pmetStatus == "Yes" && employmentStatus != "unemployed")
            ) {
              checkIfReplyEmpty();
              schemesReply = schemesReply.concat(
                "P-Max Scheme - " + schemeArray[0]
              );
            }
            if (pmetStatus == "Yes") {
              if (schemesReply.length > 0) {
                ctx.reply(schemesReply, Extra.webPreview(false));
                ctx.replyWithMarkdown(
                  "*On top of that, you are eligible for PCP schemes.*\n" +
                    "PCP is a Career Conversion Programme" +
                    "\n\nWhat *industry* would you be interesd in for PCP? \nYou can ask for *'suggestions'* if you want to know what industries we cover.\n\nIf you would like to stop Schemes Enquiry, please type 'exit'."
                );
                schemesReply = "";
                console.log("**Enter PCP Scheme 1 **");
              } else {
                // Only eligible for PCP schemes
                ctx.replyWithMarkdown(
                  "*On top of that, you are eligible for PCP schemes.*\n" +
                    "PCP is a Career Conversion Programme" +
                    "\n\nWhat *industry* would you be interesd in for PCP? \nYou can ask for *'suggestions'* if you want to know what industries we cover.\n\nIf you would like to stop Schemes Enquiry, please type 'exit'."
                );
                schemesReply = "";
                empty = false;
                console.log("**Enter PCP Scheme 2 **");
              }
              runSample(projectId, "I am eligible for PCP"); // enter into INDUSTRY intent first.
              return ctx.wizard.next();
            } else {
              ctx.reply(schemesReply, Extra.webPreview(false));
              schemesReply = "";
              empty = true;
              console.log("**End***");
              return ctx.scene.leave();
            }
          }
          if (empty) {
            schemesReply = schemesReply.concat(
              "Sorry no schemes are available for you.\nSchemes Enquiry terminated."
            );
            console.log("Nothinggggg");
            return ctx.scene.leave();
          }
        } else if (
          typeof dfReply == "string" &&
          dfReply.indexOf("Your industry") > -1
        ) {
          console.log("***FOUND IT ****");

          return ctx.scene.leave();
        } else {
          ctx.reply(dfReply +"\n\nIf you would like to stop Schemes Enquiry, please type 'exit'.");
        }
      }
    );
  },
  async ctx => {
    setReply = "";
    if(ctx.wizard.state.keyword.toLowerCase().indexOf("exit") === 0){
      ctx.replyWithMarkdown("Schemes Enquiry has been terminated.");
      ctx.scene.leave()
    }
    ctx.wizard.state.keyword = ctx.message.text;
    response = runSample(projectId, ctx.wizard.state.keyword).then(
      parameters => {
        if (dfReply.indexOf("Your industry") >= 0) {
          console.log("Industry registered");
          //Response captured
          columns = "*";
          var params = dfReply.slice(14);
          console.log(params);

          apiLink = "http://localhost:3000/scheme/pcp?industry=" + params;
          console.log(apiLink)

          request(apiLink, async function(error, res, body) {
            // console.log(error)
            // console.log(res)
            if (!error && res.statusCode == 200) {
              console.log("Request Called");
              content = JSON.parse(body);
              console.log(content.pcp[0].title);
              var limit = content.pcp.length
              jobArr = [];
              
                var jobDetails = "";
                jobDetails =
                  jobDetails + "*Title: *" + content.pcp[0].title + "\n";
                jobDetails =
                  jobDetails + "*Summary: *" + content.pcp[0].intro + "\n";
                jobDetails =
                  jobDetails + "*Duration: *" + content.pcp[0].duration + "\n";
                jobDetails =
                  jobDetails +
                  "*Part-Time / Full-Time: *" +
                  content.pcp[0].full_part_time +
                  "\n";
                  jobDetails =
                  jobDetails +
                  "*Industry: *" +
                  content.pcp[0].industry +
                  "\n";
                // jobDetails =
                //   jobDetails +
                //   "*View more here:  *" +
                //   content.pcp[0].link +
                //   "\n";
                // jobDetails =
                //   jobDetails +
                //   "*#*" + "1/3" + limit
                //   "\n";

                jobArr.push(jobDetails);
                // ctx.replyWithMarkdown(jobDetails);
              }
              var back_message = 'sc_bck_0_' + limit + '_' + params.substring(0,4) 
              var next_message = 'sc_nxt_0_' + limit + '_' + params.substring(0,4)  
              // var next_message = "sc_next_" + record_id + "_0";
              await ctx.replyWithMarkdown(jobDetails, Markup.inlineKeyboard([
                Markup.callbackButton('<', back_message),
                Markup.urlButton('Details', content.pcp[0].link),
                Markup.callbackButton('>', next_message)
              ]).extra())

            console.log("*** Scene Leave. End of FAQ ***");
            ctx.scene.leave();
          });
        } else if (userQuery.toLowerCase().indexOf("sugges") >= 0) {
          // ask again!
          suggestions =
            "*These are the industries we have:*\nInfocomm Technology,  Aerospace,  Air Transport, \nAttractions,  Built Environment,  Design, \nEducation,  Electronics,  Energy and Chemicals, \nFinancial Sservices,  Food Manufacturing,  Food Services, \nGeneral Manufacturing,  Healthcare,  Logistics\nMarine,  Medical Technology,  MICE, \nPharmaceuticals & Biotechnology,  PrecisionEngineering, \nProfessional Services,  Security,  Public Transport, \nRetail,  Social Service,  Waste Management, \n Sea Transport,  Wholesale Trade,  Water & Environment.\n\nIf you would like to stop Schemes Enquiry, please type 'exit'.";
          ctx.replyWithMarkdown(suggestions);
        } else {
          if (userQuery.toLowerCase().indexOf("food") >= 0) {
            setReply = "Do you mean 'Food Manufacturing' or 'Food Services'?\n";
          } else if (userQuery.toLowerCase().indexOf("medical") >= 0) {
            setReply = "Do you mean 'Medical Technology' or Healthcare'?\n";
          }
          setReply = setReply + "\n" + dfReply + "\n\nIf you would like to stop Schemes Enquiry, please type 'exit'.";
          ctx.replyWithMarkdown(setReply);
          setReply = "";
        }
      }
    );
  }
  // }
);

module.exports = govTechFAQ;
