require('dotenv').config()
const Telegraf = require('telegraf')
const external_api_caller = require('../api/api')
const Markup = require('telegraf/markup')
const WizardScene = require('telegraf/scenes/wizard')
const Stage = require('telegraf/stage')
const session = require('telegraf/session')
const axios = require('axios')

const skill_add = new WizardScene(
    'add_skills',
    ctx => {
      console.log(ctx.scene.ctx)
      ctx.replyWithMarkdown(
        'Please enter a skill (e.g. Software Engineer): ', 
      ) 
      return ctx.wizard.next()
    },
    ctx => { 
      const skill_add =  ctx.message.text
      var api = process.env.BACKEND_SERVER + 'skills/match'
      var body = {
        skill: skill_add,
        
      }
      external_api_caller
      .post_api(api,body)
      .then(response => {
     
console.log(response.response_code)
if (response.response_code == 200) {
 var skills = response.skills
var message = '*Do you mean:* \n\n'
 for(each in skills) {
     if(each <= 4) {
    message += "*" + (parseInt(each)+1) + ".* " + skills[each].skill + '\n'
 }
} 
message+= '\n*Enter the number corresponding to your choice!*'
ctx.replyWithMarkdown(message,  
Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([
Markup.callbackButton('Retry', 'sk_add'),
Markup.callbackButton('1', 'sk_choose_1_' + skills[0].id),
Markup.callbackButton('2', 'sk_choose_2_' + skills[1].id),
Markup.callbackButton('3', 'sk_choose_3_' + skills[2].id),
Markup.callbackButton('4', 'sk_choose_4_' + skills[3].id),
Markup.callbackButton('5', 'sk_choose_5_' + skills[4].id)
 ])))
} 
      }).catch(error => {
        ctx.reply('Error retrieving skill')
      })


      return ctx.scene.leave()
      
    }
  )

  module.exports = skill_add
