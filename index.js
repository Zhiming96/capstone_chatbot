  require('dotenv').config()
  const Telegraf = require('telegraf')
  const external_api_caller = require('./api/api')
  const Markup = require('telegraf/markup')
  const WizardScene = require('telegraf/scenes/wizard')
  const Stage = require('telegraf/stage')
  const session = require('telegraf/session')
  const axios = require('axios')
const job_searcher = require('./scenes/search_jobs')
const skill_viewer = require('./scenes/view_skills.js')
const bookmarks_viewer = require('./scenes/view_bookmarks.js')
const skill_add = require('./scenes/add_skills.js')
const govTechFAQ = require('./scenes/govtech_FAQ')

  const bot = new Telegraf(process.env.BOT_TOKEN)
  const stage = new Stage();
  stage.register(job_searcher);
  stage.register(skill_viewer)
  stage.register(bookmarks_viewer)
  stage.register(skill_add)
  stage.register(govTechFAQ)
  bot.use(session());
  bot.use(stage.middleware());
 
  bot.command('schemes',ctx=>{
    ctx.scene.enter('govTechFAQ')
  })
  bot.telegram.getMe().then(bot_informations => {
    bot.options.username = bot_informations.username
    console.log('Server has initialized bot: ' + bot_informations.username)
  })
  bot.command('testabc',  ctx => {
    console.log(ctx)
    var api = process.env.BACKEND_SERVER + 'auth/login';
    var body = {
    identifier: "system_admin",
    password: "password"
    }
    external_api_caller
    .post_api(api,body)
    .then(response => {
console.log(response)
var api = process.env.BACKEND_SERVER + 'auth/user';
var header = {
Authorization: "Token "+response.user.token
}
var body = {
  telegram_id: '694012702'
}
external_api_caller
.post_api_header(api,body,header)
.then(response2 => {
console.log(response2)

})
    })
  })
  bot.start(ctx => {
    console.log( ctx.chat)
    var api = process.env.BACKEND_SERVER + 'user/register';
    var body = {
      telegram_id: ctx.message.from.id,
      
      source: 'telegram'
    }
    external_api_caller
    .post_api(api,body)
    .then(response => {
      var message ='Registration successful. Hello ' + ctx.message.from.first_name + '!\n\n'
      message += 'Here is a list of functions that I provide! \n\n'
      message += '/searchjobs - Start looking for that dream job!\n'
      message += '/bookmarks - View a list of your bookmarked jobs!\n'
      message += '/skills - View a list of your registered skills!\n'
      message += '/profile - View your profile!\n\n'
message += 'To unlock all functionalities, head over to our <a href="'+ process.env.REACT_SERVER + "/auth/signup/" + ctx.message.from.id + '">web app</a> to register!'
console.log(message)
          ctx.reply(message, {parse_mode: "HTML"})
        })
        .catch(error => {
          ctx.reply('Error registering user!')
        })
    ctx.reply('Registering...')
  })
 
  bot.help(ctx => {
    var message ='Hello ' + ctx.message.from.first_name + '!\n\n'
    message += 'Here is a list of functions that I provide! \n\n'
    message += '/searchjobs - Start looking for that dream job!\n'
    message += '/bookmarks - View a list of your bookmarked jobs!\n'
    message += '/skills - View a list of your registered skills!\n'
    message += '/profile - View your profile!'
        ctx.reply(message)
  })

  bot.command('articles',  ctx => {
    var api = process.env.BACKEND_SERVER + 'article/cust?index=0&telegram_id=' + ctx.message.from.id 
    console.log(api)
  
     external_api_caller
         .call_api(api)
         .then(async response => {
ctx.replyWithMarkdown('*Here are the latest articles:*')
var message = "*Title:* "+response.articles[0].title + '\n\n'
 message += "*Extract:\n*"
 message += "- " + response.articles[0].sentence1+  '\n\n'
 message += "- " + response.articles[0].sentence2+  '\n\n'
 message += "- " + response.articles[0].sentence3 +  '\n\n'
 message += "*Read Time:* " + response.articles[0].readtime + " minutes"
 var next_message = 'ar_nxt_0'
 ctx.replyWithMarkdown(message, Markup.inlineKeyboard([
 
  Markup.urlButton('Read', response.articles[0].link),
  Markup.callbackButton('>', next_message),
   ]).extra())
         })

  })

  bot.command('events',  ctx => {
    var api = process.env.BACKEND_SERVER + 'event/cust?index=0&telegram_id=' + ctx.message.from.id 
    console.log(api)
  
     external_api_caller
         .call_api(api)
         .then( async response => {
await ctx.replyWithMarkdown('*Here are the latest upcoming events:*')
console.log(response)
var message = "*Title:* "+response.events[0].eventTitle + '\n\n'

 message += "*Venue:* " + response.events[0].venue+  '\n\n'
 message += "*Date/Time:* " + response.events[0].dateTime+  '\n\n'

 var next_message = 'ev_nxt_0'
 ctx.replyWithMarkdown(message, Markup.inlineKeyboard([
 
  Markup.urlButton('View', response.events[0].url),
  Markup.callbackButton('>', next_message),
   ]).extra())
         })

  })

  bot.command('skills',  ctx => {
    var api = process.env.BACKEND_SERVER + 'skills/all?source=telegram&telegram_id=' + ctx.message.from.id 
   console.log(api)
 
    external_api_caller
        .call_api(api)
        .then(async response => {
     //     console.log(response.skill_list)
 if(response.skill_list != null && response.skill_list.length != 0) {
          var message = '*Here is a list of your saved skills:*\n'
           // console.log(response.skill_list[0])
          for(var idx = 0; idx < response.skill_list.length ;idx++) {
       //     console.log(response.skill_list[0])
  message += '*' + (idx+1) + '.* ' + response.skill_list[idx].skill + '\n'
          }
          await   ctx.replyWithMarkdown(message)
          ctx.scene.enter('view_skills')


        } else {
          var message = '*No skills saved!*\n'
       await  ctx.replyWithMarkdown(message)
          ctx.scene.enter('view_skills')
        }
        })
        .catch(error => {
          console.log(error)
          ctx.replyWithMarkdown('*Error retrieving skills!*')
        })
    ctx.replyWithMarkdown('*Retrieving skills...*')
  
  })

  bot.command('searchjobs', ctx => {
    ctx.scene.enter('search_jobs')
  }) 

  
  bot.command('bookmarks', async ctx => {
    var list = '['


 api = process.env.BACKEND_SERVER + 'jobs/bookmarks/index?index=0&source=telegram&telegram_id=' + ctx.message.from.id 
 external_api_caller
 .call_api(api)
 .then(async response => { 

  if(response.job_details.length != 0 ){
await ctx.replyWithMarkdown('*Here is a list of your bookmarked jobs:*') 
      var message = '' 
       // console.log(response.skill_list[0])
     
        console.log('52123233')
        console.log(response)
        //check for null bookmark list
      //  console.log(job)
    message += '*Title:* ' + response.job_details.results[0].title + '\n'
    message += '*Job Type:* '
    message += response.job_details.results[0].employmentTypes[0].employmentType + '\n'
  console.log('2123')
  message += '*Company:* ' 
  message +=  response.job_details.results[0].metadata.isHideEmployerName
    ? 'Employer Undisclosed\n'
    : (posted_company = response.job_details.results[0].postedCompany.name) + '\n'
  console.log('3123')
  message += '*Salary:* ' 
  message +=  response.job_details.results[0].metadata.isHideSalary
    ? 'Salary Undisclosed\n'
    : (salary =
        'S$' +
        response.job_details.results[0].salary.minimum +
        ' - ' +
        'S$' +
        response.job_details.results[0].salary.maximum) + '\n'
  console.log('4123')
  
  message += '\n\n'

var back_message = 'bk_bck_0' 
var next_message = 'bk_nxt_0' 
await  ctx.replyWithMarkdown(message, Markup.inlineKeyboard([
        Markup.callbackButton('<', back_message),
        Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.job_details.results[0].uuid),
        Markup.callbackButton('>', next_message),
        Markup.callbackButton('Remove Bookmark', 'bk_rem_' + response.job_details.results[0].uuid),
//careful about bookmark_id implementation in database
      ]).extra())
    
    //  ctx.scene.enter('view_bookmarks')
    } else {
      await ctx.replyWithMarkdown('*No bookmarks found!*') 
    }
    
    }) 
    .catch(async error => {
      console.log(error)
      await  ctx.replyWithMarkdown('*Error retrieving bookmarked jobs!*')
    })
    await ctx.replyWithMarkdown('*Retrieving bookmarked jobs...*')

  }) 

  bot.command('profile', ctx => {
    var api = process.env.BACKEND_SERVER + 'user/profile?source=telegram&telegram_id=' + ctx.message.from.id 
    console.log(api)
  
     external_api_caller
         .call_api(api)
         .then(response => {
           console.log(response)
  if(response.profile.username != null) {
           var message = '*Here is your profile:* \n\n'
           message += '*email:* ' + response.profile.email + '\n'
           message += '*username:* ' + response.profile.username+ '\n'
           message += '*first_name:* ' + response.profile.first_name+ '\n'
           message += '*last_name:* ' + response.profile.last_name+ '\n'
            // console.log(response.skill_list[0])
          console.log(response)
           ctx.replyWithMarkdown(message)
  } else {
    console.log('testing')
    var message = 'You do not have a profile with us! Please go to our <a href="' + process.env.REACT_SERVER + "/auth/signup/" + ctx.message.from.id  + '">web app</a> to create your own profile.'
    console.log(message)
    ctx.reply(message, {parse_mode: 'HTML'})
  }
         })
         .catch(error => {
           ctx.replyWithMarkdown('*Error retrieving profile!*')
         })
     ctx.replyWithMarkdown('*Retrieving profile...*')
  })
  
    

  bot.on('callback_query', async ctx => { 
    console.log('wowww')
    console.log(ctx.update.callback_query.data)
    var data = ctx.update.callback_query.data.split('_')
    console.log(data)
    //console.log(ctx)
    
    if(data[0] == 'bk') { //bookmarks
if(data[1] == 'nxt' || data[1] == 'bck') {
  var index = parseInt(data[2])
  if(data[1] == 'nxt') {
index++
  } else if (data[1] == 'bck') {
    index--
  }
  console.log(index)
  api = process.env.BACKEND_SERVER + 'jobs/bookmarks/index?index=' + index + '&source=telegram&telegram_id=' + ctx.update.callback_query.from.id
  external_api_caller
  .call_api(api)
  .then(response => { 
 
   
if(response.job_details.results.length != 0) {
       var message = '' 
        // console.log(response.skill_list[0])
      
         console.log('52123233')
         console.log(response)
         //check for null bookmark list
       //  console.log(job)
     message += '*Title:* ' + response.job_details.results[0].title + '\n'
     message += '*Job Type:* '
     message += response.job_details.results[0].employmentTypes[0].employmentType + '\n'
   console.log('2123')
   message += '*Company:* ' 
   message +=  response.job_details.results[0].metadata.isHideEmployerName
     ? 'Employer Undisclosed\n'
     : (posted_company = response.job_details.results[0].postedCompany.name) + '\n'
   console.log('3123')
   message += '*Salary:* ' 
   message +=  response.job_details.results[0].metadata.isHideSalary
     ? 'Salary Undisclosed\n'
     : (salary =
         'S$' +
         response.job_details.results[0].salary.minimum +
         ' - ' +
         'S$' +
         response.job_details.results[0].salary.maximum) + '\n'
   console.log('4123')
   
   message += '\n\n'
 
 var back_message = 'bk_bck_' + index 
 var next_message = 'bk_nxt_'+ index
 console.log(message)
       ctx.editMessageText(message,  
        Telegraf.Extra.markdown().markup((m) =>
    m.inlineKeyboard([
      Markup.callbackButton('<', back_message),
         Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.job_details.results[0].uuid),
         Markup.callbackButton('>', next_message),
         Markup.callbackButton('Remove Bookmark', 'bk_rem_' + response.job_details.results[0].uuid),
 //careful about bookmark_id implementation in database
      
    ])))
        
      
     
     //  ctx.scene.enter('view_bookmarks')
  } else {
    var back_message = 'bk_bck_' + index 
    ctx.replyWithMarkdown('*End of bookmark list!*',  
    Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
    Markup.callbackButton('<', back_message),
     ])))
  
  }
     
     }) 
     .catch(error => {
       console.log(error)
       ctx.replyWithMarkdown('*Error retrieving bookmarked jobs!*')
     })
    } else if(data[1] == 'rem') {
      var body = {
        source: 'telegram',
        uuid: data[2],
        telegram_id: ctx.update.callback_query.from.id,
      }
    
      api = process.env.BACKEND_SERVER + 'jobs/bookmarks/remove'
  external_api_caller
  .post_api(api,body)
  .then(response => { 
    console.log(response)
 if(response.response_code == '200') {
  ctx.replyWithMarkdown('*Bookmark removed successfully!*') 
 } else {
   ctx.replyWithMarkdown('*Error removing bookmark!*')
 }

 api = process.env.BACKEND_SERVER + 'jobs/bookmarks/index?index=0&source=telegram&telegram_id=' + ctx.update.callback_query.from.id,
 external_api_caller
 .call_api(api)
 .then(response => { 

  

      var message = '' 
       // console.log(response.skill_list[0])
     
        console.log('52123233')
        console.log(response)
        //check for null bookmark list
      //  console.log(job)
    message += '*Title:* ' + response.job_details.results[0].title + '\n'
    message += '*Job Type:* '
    message += response.job_details.results[0].employmentTypes[0].employmentType + '\n'
  console.log('2123')
  message += '*Company:* ' 
  message +=  response.job_details.results[0].metadata.isHideEmployerName
    ? 'Employer Undisclosed\n'
    : (posted_company = response.job_details.results[0].postedCompany.name) + '\n'
  console.log('3123')
  message += '*Salary:* ' 
  message +=  response.job_details.results[0].metadata.isHideSalary
    ? 'Salary Undisclosed\n'
    : (salary =
        'S$' +
        response.job_details.results[0].salary.minimum +
        ' - ' +
        'S$' +
        response.job_details.results[0].salary.maximum) + '\n'
  console.log('4123')
  
  message += '\n\n'

var back_message = 'bk_bck_0' 
var next_message = 'bk_nxt_0' 
ctx.editMessageText(message,  
  Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([
        Markup.callbackButton('<', back_message),
        Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.job_details.results[0].uuid),
        Markup.callbackButton('>', next_message),
        Markup.callbackButton('Remove Bookmark', 'bk_rem_' + response.job_details.results[0].uuid),
//careful about bookmark_id implementation in database
      ])))
    
    //  ctx.scene.enter('view_bookmarks')

    
    }) 
    .catch(error => {
      console.log(error)
      ctx.replyWithMarkdown('*Error retrieving bookmarked jobs!*')
    })

}).catch(err => {
  console.log(err)
})
    } else if (data[1] == 'add') {
      var body = {
        source: 'telegram',
        uuid: data[2],
        telegram_id: ctx.update.callback_query.from.id,
      }
    
      api = process.env.BACKEND_SERVER + 'jobs/bookmarks/add'
  external_api_caller
  .post_api(api,body)
  .then(response => { 
    console.log(response)
 if(response.response_code == '200') {
  ctx.replyWithMarkdown('*Bookmark added successfully!*') 
 } else {
   ctx.replyWithMarkdown('*Error adding bookmark!*')
 }
}).catch(err => {
  console.log(err)
})
    }
} else if (data[0] == 'sj') {//search jobs

  var index = data[3]
if(data[1] == 'bck') {
  index--;
 
} else if (data[1] == 'nxt') {
index++;
  
}
var record_id = data[2]
var body = {
  telegram_id: ctx.update.callback_query.from.id,
  record_id: record_id,
  index: index
}
var api = process.env.BACKEND_SERVER + 'search/id'
external_api_caller
.post_api(api,body)
.then(response => {
  // important info
  //console.log(response);
  if(response.results.length != 0) {
  var message = (title = response.results[0].title) + '\n'
  console.log(response.results[0].employmentTypes[0].employmentType) // only 1 for this array?
  message +=
    response.results[0].employmentTypes[0].employmentType + '\n'
  console.log('2123')
  message += response.results[0].metadata.isHideEmployerName
    ? 'Employer Undisclosed\n'
    : (posted_company = response.results[0].postedCompany.name) + '\n'
  console.log('3123')
  message += response.results[0].metadata.isHideSalary
    ? 'Salary Undisclosed\n' 
    : (salary =
        'S$' +
        response.results[0].salary.minimum +
        ' - ' +
        'S$' +
        response.results[0].salary.maximum) + '\n'
  console.log('4123')
  message += '\n\n'
  console.log(message)

 var back_message = 'sj_bck_' +record_id + '_' + index
var next_message = 'sj_nxt_' + record_id +'_' + index

if(index == 0) {

  
  ctx.editMessageText(message,  
    Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([
   // Markup.callbackButton('<', back_message),
    Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
    Markup.callbackButton('>', next_message),
    Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
   ])))
} else if(index == 19) {
  
  ctx.editMessageText(message,  
    Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([
    Markup.callbackButton('<', back_message),
    Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
 //   Markup.callbackButton('>', next_message),
    Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
   ])))
} else {
  
 var back_message = 'sj_bck_' +record_id + '_' + index
  ctx.editMessageText(message,  
    Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([
    Markup.callbackButton('<', back_message),
    Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
    Markup.callbackButton('>', next_message),
    Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
   ])))
}
/*
for (job in response.results) {
console.log(job)
message += (title = response.results[job].title) + '\n'
console.log(response.results[job].employmentTypes[0].employmentType) // only 1 for this array?
message +=
  response.results[job].employmentTypes[0].employmentType + '\n'
console.log('2123')
message += response.results[job].metadata.isHideEmployerName
  ? 'Employer Undisclosed\n'
  : (posted_company = response.results[job].postedCompany.name) + '\n'
console.log('3123')
message += response.results[job].metadata.isHideSalary
  ? 'Salary Undisclosed\n'
  : (salary =
      'S$' +
      response.results[job].salary.minimum +
      ' - ' +
      'S$' +
      response.results[job].salary.maximum) + '\n'
console.log('4123')
message += '\n\n'
console.log(message)
 }
*/
//    message +=   (status = response.results[job].status.jobStatus) + "\n";

// message +=   (category = response.results[job].categories[job].category) + "\n"; //might have more, for now take first one
// message +=   (employment_type = response.results[job].employmentTypes[job].employmentType) + "\n";

// message +=   (position_level = response.results[job].positionLevels[job].position) + "\n";
//

//         //additional info
//         var skills = [];
//         response.results[job].skills.forEach(function(skill) {
// skills.push(skill.skill);
//         })
//         var description = response.results[job].description;
//         var minimum_years_experience = response.results[job].minimumYearsExperience;
//         var other_requirements = response.results[job].otherRequirements;
//         var working_hours = response.results[job].workingHours;
//         var number_of_vacancies = response.results[job].numberOfVacancies;

// res.json(response)


} else {
  ctx.replyWithMarkdown('*End of search results!*',  
  Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([
  Markup.callbackButton('<', back_message),
   ])))

}
  
})
.catch(error => {
  console.log(error)
  ctx.replyWithMarkdown('*Error retrieving jobs*')
})





} else if (data[0] == 'we') {
  if(data[1] == 'nxt') {
var index  = parseInt(data[2]) + 1
  
    api = process.env.BACKEND_SERVER + 'work/all?source=telegram&telegram_id=' + ctx.update.callback_query.from.id
    external_api_caller
    .call_api(api)
    .then(response => { 
   
     if(response.work_experience.length != 0 ){

         var message = '' 
          // console.log(response.skill_list[0])
        
           console.log('52123233')
           console.log(index)
           console.log(response)
           //check for null bookmark list
         //  console.log(job)
       message += '*Title:* ' + response.work_experience[index].job_title + '\n'
       message += '*Start Date: * '
       message += response.work_experience[index].start_date + '\n'
    
      message += '*End Date: * '
       message += response.work_experience[index].end_date ? response.work_experience[index].end_date + '\n' : 'current' + '\n'
      console.log('2123')
     message += '*Company:* ' 
     message +=  response.work_experience[index].company_name+ '\n'
       console.log('3123')
     message += '*Description:* ' +  response.work_experience[index].description + '\n'
     console.log('4123')
     
     message += '\n\n'
   console.log(message)
   var back_message = 'we_bck_' + index 
   var next_message = 'we_nxt_' + index
   if(index == 0) {
    console.log('e1232hhlo')
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
     // Markup.callbackButton('<', back_message),
    //  Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
      Markup.callbackButton('>', next_message),
    //  Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
     ])))
        } else if (index== response.work_experience.length-1) {
          console.log('ehasdsdshlo')
          ctx.editMessageText(message,  
            Telegraf.Extra.markdown().markup((m) =>
        m.inlineKeyboard([
            Markup.callbackButton('<', back_message),
        //    Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
         //   Markup.callbackButton('>', next_message),
         //   Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
           ])))
        } else if (response.work_experience.length == 1 ) {
          console.log('ehhsdfdflo')
          ctx.editMessageText(message)
        } else {

          console.log('ehhlo')
          ctx.editMessageText(message,  
            Telegraf.Extra.markdown().markup((m) =>
        m.inlineKeyboard([
            Markup.callbackButton('<', back_message),
      //      Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
           Markup.callbackButton('>', next_message),
       //     Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
           ])))
        }
       //  ctx.scene.enter('view_bookmarks')
       } else {
        ctx.replyWithMarkdown('*No work experience saved!*')
       }
       
       }) 
       .catch(error => {
         console.log(error)
         ctx.replyWithMarkdown('*Error retrieving work experience!*')
       })

  } else if (data[1] == 'bck') {

   var index = parseInt(data[2]) - 1


    api = process.env.BACKEND_SERVER + 'work/all?source=telegram&telegram_id=' + ctx.update.callback_query.from.id
    external_api_caller
    .call_api(api)
    .then(response => { 
   
     if(response.work_experience.length != 0 ){

         var message = '' 
          // console.log(response.skill_list[0])
        
           console.log('52123233')
           console.log(response)
           //check for null bookmark list
         //  console.log(job)
       message += '*Title:* ' + response.work_experience[index].job_title + '\n'
       message += '*Start Date: * '
       message += response.work_experience[index].start_date + '\n'
    
      message += '*End Date: * '
       message += response.work_experience[index].end_date ? response.work_experience[0].end_date + '\n' : 'current' + '\n'
      console.log('2123')
     message += '*Company:* ' 
     message +=  response.work_experience[index].company_name+ '\n'
       console.log('3123')
     message += '*Description:* ' +  response.work_experience[index].description + '\n'
     console.log('4123')
     
     message += '\n\n'
   
   var back_message = 'we_bck_' + index 
   var next_message = 'we_nxt_' + index
   if(index == 0) {
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
     // Markup.callbackButton('<', back_message),
    //  Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
      Markup.callbackButton('>', next_message),
    //  Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
     ])))
        } else if (index== response.work_experience.length-1) {
          ctx.editMessageText(message,  
            Telegraf.Extra.markdown().markup((m) =>
        m.inlineKeyboard([
            Markup.callbackButton('<', back_message),
        //    Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
         //   Markup.callbackButton('>', next_message),
         //   Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
           ])))
        } else if (response.work_experience.length == 1 ) {
          ctx.editMessageText(message)
        } else {
          ctx.editMessageText(message,  
            Telegraf.Extra.markdown().markup((m) =>
        m.inlineKeyboard([
            Markup.callbackButton('<', back_message),
      //      Markup.urlButton('Details', 'https://www.mycareersfuture.sg/job/'+response.results[0].uuid),
           Markup.callbackButton('>', next_message),
       //     Markup.callbackButton('Add to Bookmark', 'bk_add_' + response.results[0].uuid),
           ])))
        }
       //  ctx.scene.enter('view_bookmarks')
       } else {
        ctx.replyWithMarkdown('*No work experience saved!*')
       }
       
       }) 
       .catch(error => {
         console.log(error)
         ctx.replyWithMarkdown('*Error retrieving work experience!*')
       })

 
  }

} else if (data[0] == 'sk') {//skills
if(data[1] == 'add') {
  ctx.scene.enter('add_skills')
} else if (data[1] == 'choose') {
 
        var api = process.env.BACKEND_SERVER + 'skills/add'
        var body = {
          skill_add: data[3],
          telegram_id: ctx.update.callback_query.from.id,
          source: 'telegram'
        }
        external_api_caller
        .post_api(api,body)
        .then(response => {
       
console.log(response.response_code)
  if (response.response_code == 200) {
    ctx.reply('Skill ' + data[2] + ' added successfully!')
  } else if (response.response_code != 500) {
    ctx.reply(response.response_message)
  } else {
    ctx.reply('Error adding skill')
  }
        }).catch(error => {
          ctx.reply('Error adding skill')
        })
      
}
} else if(data[0] == 'ar'){//articles
  if(data[1] == 'nxt') {
    var index = (parseInt(data[2])+1)%3
    var api = process.env.BACKEND_SERVER + 'article/cust?telegram_id=' +  ctx.update.callback_query.from.id + '&index='+index
    console.log(api)
  
     external_api_caller
         .call_api(api)
         .then(async response => {
          var message = "*Title:* "+response.articles[0].title + '\n\n'
          message += "*Extract:\n*"
          message += "- " + response.articles[0].sentence1+  '\n\n'
          message += "- " + response.articles[0].sentence2+  '\n\n'
          message += "- " + response.articles[0].sentence3 +  '\n\n'
          message += "*Read Time:* " + response.articles[0].readtime + " minutes"
var back_message = 'ar_bck_' + index
 var next_message = 'ar_nxt_' + index
 if(index == 0){
  ctx.editMessageText(message,  
    Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([

 // Markup.callbackButton('<', back_message),
    Markup.urlButton('Read', response.articles[0].link),
    Markup.callbackButton('>', next_message),

   ])))
  } else if (index ==1){
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
  
    Markup.callbackButton('<', back_message),
      Markup.urlButton('Read', response.articles[0].link),
      Markup.callbackButton('>', next_message),
  
     ])))
  }else if (index ==2){
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
  
    Markup.callbackButton('<', back_message),
      Markup.urlButton('Read', response.articles[0].link),
    //  Markup.callbackButton('>', next_message),
  
     ])))
  }
         })
  } else if(data[1] == 'bck') {
    var index = (parseInt(data[2])-1)
    if(index < 0){
      index += 3
    }
    var api = process.env.BACKEND_SERVER + 'article/cust?telegram_id=' +  ctx.update.callback_query.from.id + '&index='+index
    console.log(api)
  
     external_api_caller
         .call_api(api)
         .then(async response => {
          var message = "*Title:* "+response.articles[0].title + '\n\n'
          message += "*Extract:\n*"
          message += "- " + response.articles[0].sentence1+  '\n\n'
          message += "- " + response.articles[0].sentence2+  '\n\n'
          message += "- " + response.articles[0].sentence3 +  '\n\n'
          message += "*Read Time:* " + response.articles[0].readtime + " minutes"
var back_message = 'ar_bck_' + index
 var next_message = 'ar_nxt_' + index
 if(index == 0){
  ctx.editMessageText(message,  
    Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([

 // Markup.callbackButton('<', back_message),
    Markup.urlButton('Read', response.articles[0].link),
    Markup.callbackButton('>', next_message),

   ])))
  } else if (index ==1){
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
  
    Markup.callbackButton('<', back_message),
      Markup.urlButton('Read', response.articles[0].link),
      Markup.callbackButton('>', next_message),
  
     ])))
  }else if (index ==2){
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
  
    Markup.callbackButton('<', back_message),
      Markup.urlButton('Read', response.articles[0].link),
    //  Markup.callbackButton('>', next_message),
  
     ])))
  }
         })
  }
} else if(data[0] == 'sc'){
  console.log("Schemes call back")
  if(data[1] == 'nxt' || data[1] == 'bck') {
    var index = parseInt(data[2])
    if(data[1] == 'nxt') {
  index++
    } else if (data[1] == 'bck') {
        index--     
    }
    schemeLimit = parseInt(data[3])
    schemeIndex = index % schemeLimit
    if(schemeIndex == -1){
      schemeIndex = 2
    }
    schemeIndustry = data[4]
    // console.log('Scheme Index : ' + schemeIndex)
    // console.log('Scheme Limit : ' + schemeLimit)
    api = process.env.BACKEND_SERVER + `scheme/pcp?industry=` + schemeIndustry + `%&index=` + schemeIndex
    external_api_caller
    .call_api(api)
    .then(response => {
      // if(response.pcp[0]){
        
      // }
      schemeIndex = schemeIndex + 1
      var jobDetails = "";
      jobDetails = jobDetails + "*Title: *" + response.pcp[0].title + "\n";
      jobDetails = jobDetails + "*Summary: *" + response.pcp[0].intro + "\n";
      jobDetails = jobDetails + "*Duration: *" + response.pcp[0].duration + "\n";
      jobDetails = jobDetails + "*Part-Time / Full-Time: *" + response.pcp[0].full_part_time + "\n";
      // jobDetails = jobDetails + "*#*" + schemeIndex + " / " + schemeLimit 

      var back_message = 'sc_bck_'+ schemeIndex + "_" + schemeLimit + '_' + schemeIndustry 
      var next_message = 'sc_nxt_' + schemeIndex + "_" + schemeLimit + '_' + schemeIndustry
      
      ctx.editMessageText(jobDetails,
        Telegraf.Extra.markdown().markup((m) =>
        m.inlineKeyboard([
          Markup.callbackButton('<', back_message),
          Markup.urlButton('Details', response.pcp[0].link),
          Markup.callbackButton('>', next_message),
        ])))
    })
  }
}else if(data[0] == 'ev'){//event
  if(data[1] == 'nxt') {
    var index = (parseInt(data[2])+1)%3
    var api = process.env.BACKEND_SERVER + 'event/cust?telegram_id=' +  ctx.update.callback_query.from.id + '&index='+index
    console.log(api)
  
     external_api_caller
         .call_api(api)
         .then(async response => {
          var message = "*Title:* "+response.events[0].eventTitle + '\n\n'

          message += "*Venue:* " + response.events[0].venue+  '\n\n'
          message += "*Date/Time:* " + response.events[0].dateTime+  '\n\n'
         

var back_message = 'ev_bck_' + index
 var next_message = 'ev_nxt_' + index
 if(index == 0){
  ctx.editMessageText(message,  
    Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([

 // Markup.callbackButton('<', back_message),
    Markup.urlButton('View', response.events[0].url),
    Markup.callbackButton('>', next_message),

   ])))
  } else if (index ==1){
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
  
    Markup.callbackButton('<', back_message),
      Markup.urlButton('View', response.events[0].url),
      Markup.callbackButton('>', next_message),
  
     ])))
  }else if (index ==2){
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
  
    Markup.callbackButton('<', back_message),
      Markup.urlButton('View', response.events[0].url),
    //  Markup.callbackButton('>', next_message),
  
     ])))
  }
         })
  } else if(data[1] == 'bck') {
    var index = (parseInt(data[2])-1)
    if(index < 0){
      index += 3
    }
    var api = process.env.BACKEND_SERVER + 'event/cust?telegram_id=' +  ctx.update.callback_query.from.id + '&index='+index
    console.log(api)
  
     external_api_caller
         .call_api(api)
         .then(async response => {
          var message = "*Title:* "+response.events[0].eventTitle + '\n\n'

          message += "*Venue:* " + response.events[0].venue+  '\n\n'
          message += "*Date/Time:* " + response.events[0].dateTime+  '\n\n'
         

var back_message = 'ev_bck_' + index
 var next_message = 'ev_nxt_' + index
 if(index == 0){
  ctx.editMessageText(message,  
    Telegraf.Extra.markdown().markup((m) =>
m.inlineKeyboard([

 // Markup.callbackButton('<', back_message),
    Markup.urlButton('View', response.events[0].url),
    Markup.callbackButton('>', next_message),

   ])))
  } else if (index ==1){
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
  
    Markup.callbackButton('<', back_message),
      Markup.urlButton('View', response.events[0].url),
      Markup.callbackButton('>', next_message),
  
     ])))
  }else if (index ==2){
    ctx.editMessageText(message,  
      Telegraf.Extra.markdown().markup((m) =>
  m.inlineKeyboard([
  
    Markup.callbackButton('<', back_message),
      Markup.urlButton('View', response.events[0].url),
    //  Markup.callbackButton('>', next_message),
  
     ])))
  }
         })
  }
}
    

      })

  bot.command('test', (ctx) => {
    ctx.reply('<a href="www.localhost:3001">Click here</a>', {parse_mode: 'HTML'})
  })

  
  bot.command('work', (ctx) => {
   
 api = process.env.BACKEND_SERVER + 'work/all?source=telegram&telegram_id=' + ctx.message.from.id 
 external_api_caller
 .call_api(api)
 .then(response => { 

  if(response.work_experience.length != 0 ){
ctx.replyWithMarkdown('*Here is a list of your work experience*') 
      var message = '' 
       // console.log(response.skill_list[0])
     
        console.log('52123233')
        console.log(response)
        //check for null bookmark list
      //  console.log(job)
    message += '*Title:* ' + response.work_experience[0].job_title + '\n'
    message += '*Start Date: * '
    message += response.work_experience[0].start_date + '\n'
 
   message += '*End Date: * '
    message += response.work_experience[0].end_date ? response.work_experience[0].end_date + '\n' : 'current' + '\n'
   console.log('2123')
  message += '*Company:* ' 
  message +=  response.work_experience[0].company_name+ '\n'
    console.log('3123')
  message += '*Description:* ' +  response.work_experience[0].description + '\n'
  console.log('4123')
  
  message += '\n\n'

var back_message = 'we_bck_0' 
var next_message = 'we_nxt_0' 
if(response.work_experience.length == 1) {
  ctx.replyWithMarkdown(message)
} else {
  ctx.replyWithMarkdown(message, Markup.inlineKeyboard([
    //     Markup.callbackButton('<', back_message),
         Markup.callbackButton('>', next_message)
 //careful about bookmark_id implementation in database
       ]).extra())
}
     
    
    //  ctx.scene.enter('view_bookmarks')
    } else {
      ctx.replyWithMarkdown('*No work experience found!*') 
    }
    
    }) 
    .catch(error => {
      console.log(error)
      ctx.replyWithMarkdown('*Error retrieving work experience!*')
    })
ctx.replyWithMarkdown('*Retrieving work experience...*')
  })
  bot.launch()

  //Test for First Commit
  // Testing Second Commit, link to issues